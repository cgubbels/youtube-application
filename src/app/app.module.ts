import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';
import { SearchVideosComponent } from './search-videos.component';
import { VideoListComponent } from './video-list.component';
import { VideoPlayerComponent } from './video-player.component';



@NgModule({
	imports:      [
		BrowserModule,
		FormsModule,
		HttpModule
	],
	declarations: [
		AppComponent,
  		SearchVideosComponent,
  		VideoListComponent,
  		VideoPlayerComponent
	],
	bootstrap:    [ AppComponent ]
})
export class AppModule { }