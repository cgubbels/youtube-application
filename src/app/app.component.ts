import { Component } from '@angular/core';
import { Video } from './video';
import { YoutubeService } from './youtube.service';
import { FavoritesService } from './favorites.service';

@Component({
	selector: 'youtube-app',
	templateUrl: './app.component.html',
	providers: [YoutubeService, FavoritesService]
})

export class AppComponent {
	selectedVideo: Video;
	favoritesList: Video[];

	constructor(
		private youtubeService: YoutubeService, 
		private favoritesService: FavoritesService) {
		
		//watch the selectedVideo for changes
		youtubeService.SelectedVideo$.subscribe(
			video => this.selectedVideo = video
		);

		//watch the favoritesList for changes
		favoritesService.Favorites$.subscribe(
			list => this.favoritesList = list
		);

		//retrieve favorites
		favoritesService.getFavorites();
	}
}
