import { Injectable} from '@angular/core';
import { Http, Response, RequestOptions, URLSearchParams } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';


import { Video } from './video';

@Injectable()
export class YoutubeService {
	private apiUrl = 'https://www.googleapis.com/youtube/v3/';
	private apiKey = 'AIzaSyCdRbhiHLbZnSbVs-Uv8ml5BnOOq3ZhNzY'; 

	private SelectedVideo = new Subject<Video>();

	SelectedVideo$ = this.SelectedVideo.asObservable();

	constructor(private http: Http) {}

	/**
	 * calls youtube search api, returns observable response
	 * @param searchParams {object} query settings
	 */
	searchVideos(searchParams={}) {
		//set default settings
		let searchOptions = {
			part: 'snippet',
			type: 'video',
			relevanceLanguage: 'en'
		};

		Object.assign(searchOptions, searchParams);

		return this.callApi(searchOptions, 'search');
	}

	/**
	 * emits an observable change to selectedVideo, then grabs video details
	 * @param video {video object} video to select
	**/
	setSelectedVideo(video: Video) {
		this.SelectedVideo.next(video);
		this.getVideoDetails(video);
	}

	/**
	 * accepts video, attaches statistics
	 * @param video {video object} video to attach statistics to
	**/
	private getVideoDetails (video: Video) {	
		this.callApi({'part': 'statistics', 'id': video.id.videoId }, 'videos').subscribe(
			details => {
				video.statistics = details[0].statistics;
				//update SelectedVideo
				this.SelectedVideo.next(video);

				//check for comments, grab them if they exists
				this.getVideoComments(video);
			});
	}

	/**
	 * accepts a video and makes http call and attaches comments to video
	 * @param video {Video object} the video to attach comments to
	**/
	private getVideoComments (video: Video) {
		//return if video has no comments
		if(video.statistics.commentCount === '0')
			return false;

		this.callApi({ 'part' : 'snippet', 'videoId': video.id.videoId}, 'commentThreads').subscribe(
			comments => {
				video.comments = comments;
				//update SelectedVideo
				this.SelectedVideo.next(video);
			})
	}

	/**
	 * takes a set of parameters and a youtube endpoint and 
	 * makes an observable http call, handling response and errors
	 * @param parameters {object} set of parameters to add to url query
	 * @param endpoint {string} endpoint of the api call
	**/
	private callApi(parameters={}, endpoint: string): Observable<any> {
		let callUrl = this.apiUrl + endpoint;
		let params = new URLSearchParams();
		
		//set search params
		params.set('key', this.apiKey);
		for (var key in parameters) {
			params.set(key, parameters[key]);
		}

		//make call
		return this.http.get(callUrl, { search: params })
			.map(this.extractData)
			.catch(this.handleErrors);
	}

	/**
	 * returns extracted array of response items from api call
	 * @param res {string} the api response
	**/
	private extractData(res: Response) {
		let response = res.json();
		return response.items || [];
	}

	/**
	 * returns generic message for api errors
	 * @param error {any} Unnecessary at this point
	**/
	private handleErrors (error: Response | any) {
		return Observable.throw('Sorry, unable to reach youtube.');
	}
}


