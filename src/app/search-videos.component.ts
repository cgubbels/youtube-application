import { Component, OnInit, NgModule } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Video } from './video';
import { YoutubeService } from './youtube.service';

@Component({
	selector: 'search-videos',
	templateUrl: './search-videos.component.html'
})

export class SearchVideosComponent implements OnInit {
	errorMessage: string;
	results: Video[];

	//default search settings
	searchSettings = {
		q: '',
		order: 'date',
		maxResults: 15
	};
	sortOptions = ['date', 'rating', 'relevance'];
	mode = 'Observable';


	constructor (private youtubeService: YoutubeService) {}

	ngOnInit() { }

	/**
	 * submits the search parameters, and handles response
	**/
	searchVideos() {
		this.youtubeService.searchVideos(this.searchSettings)
			.subscribe(
				results => this.results = results,
				error => this.errorMessage = <any>error);
	}
}
