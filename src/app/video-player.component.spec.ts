import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';

import { VideoPlayerComponent } from './video-player.component';
import { Video } from './video';
import { FavoritesService } from './favorites.service';
import { SimpleChanges, SimpleChange } from '@angular/core';

describe('VideoPlayerComponent', () => {
	let comp:    VideoPlayerComponent;
	let fixture: ComponentFixture<VideoPlayerComponent>;
	let favoritesService: FavoritesService;
	let spy: jasmine.Spy;
	


	const mockVideo = new Video('someId', {});

	const changesObj: SimpleChanges = {
	  video: new SimpleChange({}, mockVideo, true)
	};

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ VideoPlayerComponent ],
			providers: [  FavoritesService ]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(VideoPlayerComponent);
		comp = fixture.componentInstance;

		//Inject favorites service
		favoritesService = fixture.debugElement.injector.get(FavoritesService);
		favoritesService = TestBed.get(FavoritesService);

	});

	///////////////////////////////////////////
	///		Toggle Favorite Function 		///
	///////////////////////////////////////////

	it('should reverse isFavorite value', () => {
		spy = spyOn(favoritesService, 'toggleFavorite');

		comp.isFavorite = true;
		comp.toggleFavorite(mockVideo);
		expect(comp.isFavorite).toBe(false);

		//and again
		comp.toggleFavorite(mockVideo);
		expect(comp.isFavorite).toBe(true);

	});

	it('should call favoriteService.toggleFavorite', () => {
		spy = spyOn(favoritesService, 'toggleFavorite');

		comp.toggleFavorite(mockVideo);
		expect(spy).toHaveBeenCalled();
	});

	///////////////////////////////////////////
	///		On Change 				 		///
	///////////////////////////////////////////

	it('should not call favoriteService.findFavorite if no video changes', () => {
		spy = spyOn(favoritesService, 'findFavorite');
		//Changes without video changes
		comp.ngOnChanges({});

		expect(spy).not.toHaveBeenCalled();
	});

	it('should call favoriteService.findFavorite if video changes and set isFavorite based on response', () => {
		let mockResponse = 1;
		comp.isFavorite = false;

		spy = spyOn(favoritesService, 'findFavorite').and.returnValue(mockResponse);
		//Changes without video changes
		comp.ngOnChanges(changesObj);

		expect(spy).toHaveBeenCalledWith('someId');
		expect(comp.isFavorite).toBe(true);
	});

	it('should set the videoUrl', () => {
		spy = spyOn(favoritesService, 'findFavorite');

		comp.ngOnChanges(changesObj);

		expect(comp.videoUrl).toEqual('https://www.youtube.com/embed/someId');
	});

});