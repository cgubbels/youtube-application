import { FavoritesService } from './favorites.service';
import { Video } from './video';
import { Subject } from 'rxjs/Subject';

describe('Service: FavoritesService', () => {
	let service: any;

	let storageSpy: any;
	let mockStorageResponse: string;

	beforeEach(() => {
        service = new FavoritesService();

        service.FavoritesList = [
			new Video('Video1', {}),
			new Video('Video2', {}),
			new Video('Video3', {}),
			new Video('Video4', {}),
        ];

        storageSpy = {
        	getItem: spyOn(window.localStorage, 'getItem').and.callFake(function (key : string) {
			    return mockStorageResponse;
			}),
        	setItem: spyOn(window.localStorage, 'setItem')
    	};
    });

	///////////////////////////////////////
	///		findFavorite Function 		///
	///////////////////////////////////////

	it('should return index of video', () => {
		//Video in Favorites
		expect(service.findFavorite('Video2')).toEqual(1);

		//unknown video
		expect(service.findFavorite('OtherVideo')).toEqual(-1);
	});


	///////////////////////////////////////////
	///		toggleFavorite Function 		///
	///////////////////////////////////////////

	it('should add new video to the favorites list', () => {
		expect(service.FavoritesList.length).toEqual(4)

		service.toggleFavorite(new Video('New Video', {}));

		expect(service.FavoritesList.length).toEqual(5);
		expect(service.FavoritesList[4].id.videoId).toEqual('New Video');
	});

	it('should remove a video to the favorites list', () => {
		expect(service.FavoritesList.length).toEqual(4)

		service.toggleFavorite(new Video('Video2', {}));

		expect(service.FavoritesList.length).toEqual(3);
		expect(service.findFavorite('Video2')).toEqual(-1);
	});

	it('should call localStorage.setItem with the updated Favorites list', () => {

		service.toggleFavorite(new Video('Video5', {}));

		expect(storageSpy.setItem).toHaveBeenCalledWith(
			'youtubeFavorites',
			JSON.stringify(service.FavoritesList)
		);
	});


	///////////////////////////////////////////
	///		getFavorites Function	 		///
	///////////////////////////////////////////

	it('should return false if no localStorage favorites', () => {
		mockStorageResponse = '';

		expect(service.getFavorites()).toBe(false);
	});

	it('should set favoritesList to the returned array of favorites', () => {
		let newVideoList = [ 'video list' ];

		mockStorageResponse = JSON.stringify(newVideoList);

		service.getFavorites();

		expect(service.FavoritesList).toEqual(newVideoList);
	});

});
