import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By }              from '@angular/platform-browser';

import { Video } from './video';
import { SearchVideosComponent } from './search-videos.component';
import { VideoListComponent } from './video-list.component';
import { YoutubeService } from './youtube.service';

describe('SearchVideosComponent', () => {
	let comp:    SearchVideosComponent;
	let fixture: ComponentFixture<SearchVideosComponent>;
	let youtubeService: YoutubeService;
	let spy: jasmine.Spy;

	let youtubeServiceSub = {};

	beforeEach(async(() => {
		youtubeServiceSub = {
			searchVideos: true
		};

		TestBed.configureTestingModule({
			imports: [ FormsModule ],
			declarations: [ SearchVideosComponent, VideoListComponent ],
			providers: [ { provide: YoutubeService, useValue: youtubeServiceSub } ]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SearchVideosComponent);
		comp = fixture.componentInstance;

		//Inject youtubeService
		youtubeService = fixture.debugElement.injector.get(YoutubeService);
		youtubeService = TestBed.get(YoutubeService);


		spy = spyOn(youtubeService, 'searchVideos').and.returnValue({ subscribe: () => {} });
	});

	///////////////////////////////////////
	///		searchVideos Function 		///
	///////////////////////////////////////

	it('should call youtubeService searchVideos function with query params', () => {
		comp.searchSettings = {
			q: 'search string',
			order: 'date',
			maxResults: 5
		};

		comp.searchVideos();
		expect(spy).toHaveBeenCalledWith(comp.searchSettings);
	});

});
