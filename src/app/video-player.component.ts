import { Component, Input } from '@angular/core';

import { Video } from './video';
import { FavoritesService } from './favorites.service';
import { SimpleChanges } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
	selector: 'video-player',
	templateUrl: './video-player.component.html',

})

export class VideoPlayerComponent {
	@Input() video: Video;

	private baseUrl = 'https://www.youtube.com/embed/';

	isFavorite: boolean = false;
	videoUrl: string;

	constructor(private favoritesService: FavoritesService, private domSanitizer: DomSanitizer) {}

	/**
	 * If the passed video changes, we check if it is in the favorites array
	 * and validate the resourceUrl
	**/
	ngOnChanges(changes: SimpleChanges) {
		if (changes['video'] && changes['video'].currentValue) {
			let videoId = changes['video'].currentValue.id.videoId
			
			this.isFavorite = this.favoritesService.findFavorite(videoId) > -1;
			this.videoUrl = this.safeUrl(this.baseUrl + videoId);
		}
	}

	/**
	 * adds/removes the video from the favorite list
	 * @param video {video object}
	**/
	toggleFavorite(video: Video) {
		this.isFavorite = !this.isFavorite;
		this.favoritesService.toggleFavorite(video);
	}

	safeUrl(value: string, args?: any): any {
        return this.domSanitizer.bypassSecurityTrustResourceUrl(value);
    }
}