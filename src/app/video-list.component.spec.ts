import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';

import { VideoListComponent } from './video-list.component';
import { YoutubeService } from './youtube.service';
import { Video } from './video';

describe('VideoListComponent', () => {
	let comp:    VideoListComponent;
	let fixture: ComponentFixture<VideoListComponent>;
	let youtubeService: YoutubeService;
	let spy: jasmine.Spy;
	let videoListEl;
	let expectVideoList: Video[];

	let youtubeServiceSub = {};

	const mockVideo = new Video('', {	
		title: 'A video',
		description: 'Video Description',
		publishedAt: '03/24/1983',
		thumbnails: {
			default: { url: 'video-url'}
		}
	});

	beforeEach(async(() => {
		youtubeServiceSub = {
			setSelectedVideo: true
		};

		TestBed.configureTestingModule({
			declarations: [ VideoListComponent ],
			providers: [ { provide: YoutubeService, useValue: youtubeServiceSub } ]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(VideoListComponent);
		comp = fixture.componentInstance;

		//Inject youtubeService
		youtubeService = fixture.debugElement.injector.get(YoutubeService);
		youtubeService = TestBed.get(YoutubeService);

	});

	it('li count should match number of videos', () => {
		expectVideoList = [ mockVideo, mockVideo, mockVideo];
		comp.list = expectVideoList;
		fixture.detectChanges();
		videoListEl = fixture.nativeElement.querySelectorAll('li');
		expect(videoListEl.length).toBe(3);
	});

	it('should call youtubeService.setSelectedVideo when selectVideo called', () => {
		spy = spyOn(youtubeService, 'setSelectedVideo');
		comp.selectVideo(mockVideo);
		expect(spy.calls.any()).toBe(true);
	});

});