export class Video {
	id: {
		videoId: string
	};
	statistics: {
		commentCount: string
	};
	snippet: Object;
	comments: [Object];

	constructor(videoId: string, snippet: any) {
		this.id = {
			videoId: videoId
		};
		this.snippet = snippet;
	}
};