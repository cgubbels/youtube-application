import { Injectable} from '@angular/core';

import { Video } from './video';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class FavoritesService {
	private FavoritesList:  Video[] = [];
	private Favorites = new Subject<Video[]>();

	Favorites$ = this.Favorites.asObservable();

	/**
	 * grabs the favorites from the localStorage
	**/
	getFavorites() {
		let storageItem = localStorage.getItem('youtubeFavorites');

		if ( !storageItem ) 
			return false;

		this.FavoritesList = JSON.parse(storageItem);
		this.Favorites.next(this.FavoritesList);
	}

	/**
	 * if a video is not in the favorites, it adds it
	 * if it is in the favorites, it removes it
	 * @param video {video object} the video to remove/add
	**/
	toggleFavorite(video: Video) {
		let itemIndex = this.findFavorite(video.id.videoId);

		if(itemIndex < 0)
			this.FavoritesList.push(video);
		else 
			this.FavoritesList.splice(itemIndex, 1);
		
		localStorage.setItem('youtubeFavorites', JSON.stringify(this.FavoritesList));
		this.Favorites.next(this.FavoritesList);
	}

	/**
	 * returns the index of the video in the favorites array
	 * @param videoId {string}
	**/
	findFavorite(videoId: string) {
		return this.FavoritesList.findIndex( function(item) {return item.id.videoId === videoId;});
	}
}
