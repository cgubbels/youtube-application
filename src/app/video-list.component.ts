import { Component, Input } from '@angular/core';
import { Video } from './video';
import { YoutubeService } from './youtube.service';
	
@Component({
	selector: 'video-list',
	templateUrl: './video-list.component.html'
})


export class VideoListComponent {
	@Input() list: Video[];


	constructor(private youtubeService: YoutubeService) {}

	/**
	 * sets the video as selected, so that it can appear in the player
	 * @param video {video object} 
	 **/
	selectVideo(video: Video) {
		this.youtubeService.setSelectedVideo(video);
	}
}