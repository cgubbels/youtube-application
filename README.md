# Youtube Sample Application

This repository is an example of a single page youtube api application in Angular 2.

It extends the angular quickstart repository [angular.io quickstart](https://angular.io/docs/ts/latest/quickstart.html) for ease of basic start up.

The core of the application, the app.component, services and child components were all written by myself (Chris Gubbels).


## About the application

The core of this application are its two services, **YoutubeService** which handles the api requests and responses as well as setting a selected video, and the **Favorites** service which write to and retrieves data from a localStorage item.

In writting the Favorites service I choose to store the entire Video object on the LocalStorage in order to display functionality adding new videos and removing them from the storage.  I considered creating a unique youtube playlists for each application user and only storing the name of the playlist locally.  This would have saved storage space, but would have displayed very little use of LocalStorage.

I built the Youtube service around a base function to make http requests rather than repeating that functionality and made its parameters broad enough to use for the various youtube endpoints called by the application.

These two services are utilized by the **app component** and its children.  The **Search-videos** component collects the keyword and sort order from the user and calls the youtube service.  It then uses the **video list** component to display the results.  This component is used again to display the favorites list. The  **video player** component accepts a video object (the selected video) and displays the video, its metadata and a list of comments. The video player then uses the favorites service to add/remove the video from the list of favorites.
    

```
## Installation

Install the npm packages described in the `package.json` and verify that it works:

```shell
npm install
npm start
```


